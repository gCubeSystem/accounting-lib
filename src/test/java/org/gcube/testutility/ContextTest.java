/**
 * 
 */
package org.gcube.testutility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.security.Owner;
import org.gcube.common.security.secrets.GCubeSecret;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ContextTest.class);
	
	protected static Properties properties;
	protected static final String PROPERTIES_FILENAME = "token.properties";
	
	public static final String PARENT_DEFAULT_TEST_SCOPE;
	public static final String DEFAULT_TEST_SCOPE;
	public static final String ALTERNATIVE_TEST_SCOPE;
	
	public static final String DEFAULT_TEST_SCOPE_ANOTHER_USER;
	
	static {
		properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(PROPERTIES_FILENAME);
		
		try {
			// load the properties file
			properties.load(input);
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
		
		// PARENT_DEFAULT_TEST_SCOPE = "/pred4s"
		// DEFAULT_TEST_SCOPE_NAME = PARENT_DEFAULT_TEST_SCOPE + "/preprod";
		// ALTERNATIVE_TEST_SCOPE = DEFAULT_TEST_SCOPE_NAME + "/preVRE";
		
		
		PARENT_DEFAULT_TEST_SCOPE = "/gcube";
		DEFAULT_TEST_SCOPE = PARENT_DEFAULT_TEST_SCOPE + "/devNext";
		ALTERNATIVE_TEST_SCOPE = DEFAULT_TEST_SCOPE + "/NextNext";
		
		DEFAULT_TEST_SCOPE_ANOTHER_USER = "lucio.lelii_" + DEFAULT_TEST_SCOPE;
		
		try {
			setContextByName(DEFAULT_TEST_SCOPE);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String getCurrentScope(String token) throws ObjectNotFound, Exception {
		GCubeSecret secret = new GCubeSecret(token);
		String context = secret.getContext();
		logger.info("Context of token {} is {}", token, context);
		return context;
	}
	
	public static void setContextByName(String fullContextName) throws ObjectNotFound, Exception {
		String token = ContextTest.properties.getProperty(fullContextName);
		setContext(token);
	}
	
	private static void setContext(String token) throws ObjectNotFound, Exception {
		GCubeSecret secret = new GCubeSecret(token);
		Owner clientInfo = secret.getOwner();
		logger.debug("User : {} - external client? ok: {}", clientInfo.getId(), clientInfo.isExternalClient());
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(DEFAULT_TEST_SCOPE);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
	}
	
}
