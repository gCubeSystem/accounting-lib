package org.gcube.accounting.aggregator;

import java.io.InputStream;
import java.util.List;

import org.gcube.accounting.datamodel.validations.validators.MatcherReplace;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.documentstore.records.DSMapper;
import org.gcube.testutility.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HarmonizerTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(HarmonizerTest.class);
	
	@Test
	public void test() {
		RegexRulesAggregator regexRulesAggregator = RegexRulesAggregator.getInstance();
		List<MatcherReplace> list = regexRulesAggregator.getMatcherReplaceList();
		for(MatcherReplace matcherReplace : list) {
			logger.debug("{}", matcherReplace);
		}
	}
	
	@Test
	 public void testUnMarshallingMatcherReplace() throws Exception {
		InputStream inputStream = HarmonizerTest.class.getClassLoader().getResourceAsStream("rule.json");
		ObjectMapper mapper = DSMapper.getObjectMapper();
		MatcherReplace matcherReplace = mapper.readValue(inputStream, MatcherReplace.class);
		logger.debug("{}", matcherReplace);
	 }
}
