/**
 * 
 */
package org.gcube.accounting.datamodel.backwardcompatibility;

import java.io.Serializable;
import java.net.URI;

import org.gcube.accounting.datamodel.basetypes.AbstractStorageStatusRecord;
import org.gcube.accounting.datamodel.validations.validators.ValidURIValidator;
import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.records.implementation.FieldAction;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class MoveToProviderURIAction implements FieldAction {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Serializable validate(String key, Serializable value, Record record) throws InvalidValueException  {
		ValidURIValidator validURIValidator = new ValidURIValidator();
		value = validURIValidator.validate(key, value, record);
		record.setResourceProperty(AbstractStorageStatusRecord.PROVIDER_URI, (URI) value);
		return null;  //Returning null the initial key is removed from Record
	}
	
}
