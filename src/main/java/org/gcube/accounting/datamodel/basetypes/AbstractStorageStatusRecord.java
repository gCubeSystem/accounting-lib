/**
 * 
 */
package org.gcube.accounting.datamodel.basetypes;

import java.io.Serializable;
import java.net.URI;
import java.util.Map;

import org.gcube.accounting.datamodel.BasicUsageRecord;
import org.gcube.accounting.datamodel.backwardcompatibility.MoveToProviderURI;
import org.gcube.accounting.datamodel.basetypes.AbstractStorageUsageRecord.DataType;
import org.gcube.accounting.datamodel.validations.annotations.FixDataVolumeSign;
import org.gcube.accounting.datamodel.validations.annotations.ValidDataType;
import org.gcube.accounting.datamodel.validations.annotations.ValidURI;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.records.implementation.RequiredField;
import org.gcube.documentstore.records.implementation.validations.annotations.NotEmpty;
import org.gcube.documentstore.records.implementation.validations.annotations.ValidLong;

/**
 * @author Alessandro Pieve (ISTI - CNR) alessandro.pieve@isti.cnr.it
 * @author Luca Frosini (ISTI - CNR)
 */
public abstract class AbstractStorageStatusRecord extends BasicUsageRecord {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -5754343539116896036L;
	
	/**
	 * KEY for : Quantity of data in terms of KB
	 */
	@RequiredField @ValidLong @FixDataVolumeSign
	public static final String DATA_VOLUME = "dataVolume";
	
	/**
	 *  KEY for : type of data accessed. 
	 *  The value is a controlled dictionary by StorageStatusRecord.DataType
	 */
	@RequiredField @ValidDataType
	public static final String DATA_TYPE = "dataType";
		
	/**
	 * KEY for : data Count number of objects
	 */
	@RequiredField @ValidLong @NotEmpty
	public static final String DATA_COUNT = "dataCount";
	
	/**
	 * KEY for : data service class identifier
	 */
	@Deprecated
	public static final String DATA_SERVICECLASS = "dataServiceClass";
	
	/**
	 * KEY for : data service name identifier
	 */
	@Deprecated
	public static final String DATA_SERVICENAME = "dataServiceName";
	
	/**
	 * KEY for : data service name id
	 */
	@Deprecated
	public static final String DATA_SERVICEID = "dataServiceId";

	/**
	 * KEY for : providerId the identifier of the provider which is the target of a read/write operation
	 */
	@ValidURI @MoveToProviderURI
	@Deprecated
	public static final String PROVIDER_ID = "providerId";
	
	@RequiredField @ValidURI
	public static final String PROVIDER_URI = "providerURI";
	
	
	
	public AbstractStorageStatusRecord() {
		super();
	}
	
	public AbstractStorageStatusRecord(Map<String, ? extends Serializable> properties) throws InvalidValueException {
		super(properties);
	}

	private static final String ABSTRACT_TO_REPLACE = "Abstract";

	@Override
	public String getRecordType() {
		return AbstractStorageStatusRecord.class.getSimpleName().replace(ABSTRACT_TO_REPLACE, "");
	}
	
	@JsonIgnore
	public long getDataVolume() {
		return (Long) this.resourceProperties.get(DATA_VOLUME);
	}
	
	public void setDataVolume(long dataVolume) throws InvalidValueException {
		setResourceProperty(DATA_VOLUME, dataVolume);
	}
	
	@JsonIgnore
	public DataType getDataType() {
		return (DataType) this.resourceProperties.get(DATA_TYPE);
	}

	public void setDataType(DataType dataType) throws InvalidValueException {
		setResourceProperty(DATA_TYPE, dataType);
	}
	
	@JsonIgnore
	public long getDataCount() {
		return (Long) this.resourceProperties.get(DATA_COUNT);
	}

	public void setDataCount(long dataCount) throws InvalidValueException {
		setResourceProperty(DATA_COUNT, dataCount);
	}
	
	@Deprecated
	@JsonIgnore
	public String getDataServiceClass() {
		return (String) this.resourceProperties.get(DATA_SERVICECLASS);
	}
	
	@Deprecated
	public void setDataServiceClass(String dataServiceClass) throws InvalidValueException {
		setResourceProperty(DATA_SERVICECLASS, dataServiceClass);
	}
	
	@Deprecated
	@JsonIgnore
	public String getDataServiceName() {
		return (String) this.resourceProperties.get(DATA_SERVICENAME);
	}
	
	@Deprecated
	public void setDataServiceName(String dataServiceName) throws InvalidValueException {
		setResourceProperty(DATA_SERVICENAME, dataServiceName);
	}
	
	@Deprecated
	@JsonIgnore
	public String getDataServiceId() {
		return (String) this.resourceProperties.get(DATA_SERVICEID);
	}
	
	@Deprecated
	public void setDataServiceId(String dataServiceId) throws InvalidValueException {
		setResourceProperty(DATA_SERVICEID, dataServiceId);
	}
	
	@JsonIgnore
	@Deprecated
	public URI getProviderId() {
		return (URI) this.resourceProperties.get(PROVIDER_URI);
	}
	
	@Deprecated
	public void setProviderId(URI provideId) throws InvalidValueException {
		// setResourceProperty(PROVIDER_ID, provideId);
		setResourceProperty(PROVIDER_URI, provideId);
	}
	
	@JsonIgnore
	public URI getProviderURI() {
		return (URI) this.resourceProperties.get(PROVIDER_URI);
	}
	
	public void setProviderURI(URI provideURI) throws InvalidValueException {
		setResourceProperty(PROVIDER_URI, provideURI);
	}
}
